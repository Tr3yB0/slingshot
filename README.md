![Slingshot.jpg](https://bitbucket.org/repo/6oKrRL/images/4140154045-Slingshot.jpg)
# Welcome to the Slingshot Project! #

Slingshot! is an interface for managing relational databases using computational design tools such as Grasshopper and Dynamo.  Slingshot! exposes nodes to to access, modify, query, and create relational databases  using interfaces such as MySQL, ODBC, and SQLite.  

Originally, Slingshot! was first released in April 2011 as a free plug-in for Grasshopper.  Slingshot! is now an open source project under the GNU license.  Here you will find the source code for **Grasshopper and Dynamo versions** of Slingshot!.  My hope is that the concepts introduced by this plug-in will be expanded upon by collaborators and will also inform the work of other projects in the Computational Design community.

Enjoy the source! 
-Nate

### How do I get started? ###

* Visit the Dynamo "Package Manager" - http://dynamopackages.com/ to get Slingshot! for Dynamo
* Build Slingshot from the source (Visual Studio 2013 VB.NET Project)
* Depending on your use case, you will also need [MySQL .NET Connectors](https://www.nuget.org/packages/MySql.Data/) and [System.Data.SQLite](https://www.nuget.org/packages/System.Data.SQLite)

### How can I contribute? ###

If you have built some cool stuff for Slingshot (Grasshopper or Dynamo) and would like to share it back with the official project, you can follow these steps...

*  [Fork the Slingshot repo.](https://confluence.atlassian.com/display/BITBUCKET/Fork+a+Repo,+Compare+Code,+and+Create+a+Pull+Request)
*  Make cool stuff.
*  Submit a [Pull Request](https://confluence.atlassian.com/display/BITBUCKET/Work+with+pull+requestst)

### GNU License ###

Slingshot! is authored and maintained by Nathan Miller at [The Proving Ground](http://TheProvingGround.org)

    Copyright (C) 2014  Nathan Miller

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see http://www.gnu.org/licenses/